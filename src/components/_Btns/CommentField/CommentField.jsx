import React from "react";
import { useSelector } from "react-redux";
import { Formik } from "formik";

function CommentField({ postId, onCommentAdd }) {
  let user = useSelector((store) => store);

  return (
    <Formik
      initialValues={{ commentText: "" }}
      onSubmit={(values, { setSubmitting }) => {
        let comment = {
          author: {
            id: user.userId,
            nickname: user.userName,
          },
          postId: postId,
          text: values.commentText,
        };
        // console.log(comment);
        fetch("http://localhost:8081/post-add-comment/", {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(comment),
        }).then((resp) => {
          if (resp.ok) {
            onCommentAdd(comment);
          }
          //   console.log(1);
        });

        setSubmitting(false);
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
        <form onSubmit={handleSubmit} className="post__comment-form">
          <input
            className="post-comment-form__input"
            type="commentText"
            name="commentText"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.commentText}
          />

          {errors.email && touched.email && errors.email}

          {user && (
            <button
              type="submit"
              className="post-comment-form__submit-btn"
              disabled={isSubmitting}
            >
              Add a comment
            </button>
          )}
        </form>
      )}
    </Formik>
  );
}

export default CommentField;
