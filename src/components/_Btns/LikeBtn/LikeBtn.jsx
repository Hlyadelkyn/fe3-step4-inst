import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";

import "../Btn.scss";

function LikeBtn({ postId, likes }) {
  let userId = useSelector((store) => store.userId);

  let [nLikes, setNlikes] = useState(likes.length);

  let [isLiked, setIsLiked] = useState(null);

  useEffect(() => {
    setIsLiked(likes.includes(userId));
  }, [likes, userId]);

  let handleLikeBtnClick = () => {
    // console.log(userId, postId);
    fetch("http://localhost:8081/user-togglelike/", {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        userId: userId,
        postId: postId,
      }),
    });

    if (isLiked) {
      setIsLiked(() => {
        return false;
      });
      setNlikes((prevState) => {
        return prevState - 1;
      });
    } else {
      setIsLiked(() => {
        return true;
      });
      setNlikes((prevState) => {
        return prevState + 1;
      });
    }
  };
  return (
    <>
      {userId ? (
        <button
          className="post-action-button like-btn"
          onClick={handleLikeBtnClick}
          data-testid="like-button"
        >
          {isLiked ? (
            <>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 29.604 26.523"
              >
                <path
                  id="Instagram-Like-Icon-03brf3"
                  d="M88.987,68.08a7.6,7.6,0,0,0-4.141-1.249,8.032,8.032,0,0,0-6.491,3.479,8.035,8.035,0,0,0-6.491-3.479,7.6,7.6,0,0,0-4.146,1.249c-3.987,2.576-5.347,8.287-3.048,12.748,1.875,3.638,9.788,9.954,12.648,12.163a1.687,1.687,0,0,0,1.036.362,1.667,1.667,0,0,0,1.031-.362c2.861-2.209,10.778-8.525,12.656-12.163,2.3-4.461.931-10.172-3.056-12.748"
                  transform="translate(-63.553 -66.831)"
                  fill="#eb2828"
                />
              </svg>
            </>
          ) : (
            <>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 29.604 26.523"
              >
                <path
                  id="Instagram-Like-Icon-03brf3"
                  d="M88.987,68.08a7.6,7.6,0,0,0-4.141-1.249,8.032,8.032,0,0,0-6.491,3.479,8.035,8.035,0,0,0-6.491-3.479,7.6,7.6,0,0,0-4.146,1.249c-3.987,2.576-5.347,8.287-3.048,12.748,1.875,3.638,9.788,9.954,12.648,12.163a1.687,1.687,0,0,0,1.036.362,1.667,1.667,0,0,0,1.031-.362c2.861-2.209,10.778-8.525,12.656-12.163,2.3-4.461.931-10.172-3.056-12.748"
                  transform="translate(-63.553 -66.831)"
                  fill="#fafafa"
                />
              </svg>
            </>
          )}{" "}
          <div className="post-action-button-n ">{nLikes}</div>
        </button>
      ) : (
        <></>
      )}
    </>
  );
}

export default LikeBtn;
