import React from "react";
import { Link } from "react-router-dom";

import "./ToHomeBtn.scss";

function ToHomeBtn() {
  return (
    <Link className="to-home-btn" data-testid="toHome-button" to={"/home"}>
      HOME
    </Link>
  );
}
export default ToHomeBtn;
