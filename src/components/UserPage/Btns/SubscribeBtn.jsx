import React, { useState } from "react";

// import { subscribe, unsubscribe } from "../../../../server/utils/BDactions";

function SubscribeBtn({ isSubscribed: iS, id, currentUserId }) {
  let [isSubscribed, setIsSubscribed] = useState(iS);
  let handleBtnClick = () => {
    // console.log(id);
    fetch("http://localhost:8081/user-togglesubscribtion/", {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        userToToggleSubscribtionId: currentUserId,
        userToToggleSubscribtionOfId: id,
      }),
    }).then((response) => {
      if (response.ok) {
        if (isSubscribed) {
          // console.log(id + " unsubscribe");
          setIsSubscribed(false);
        } else {
          // console.log(id + " subscribe");
          setIsSubscribed(true);
        }
      }
    });
  };
  return (
    <>
      <button className="user__subsribe-btn" onClick={handleBtnClick}>
        {isSubscribed ? <>unsubscribe</> : <>subscribe</>}
      </button>
    </>
  );
}
export default SubscribeBtn;
