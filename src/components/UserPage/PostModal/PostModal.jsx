import React, { useState } from "react";
import { Link } from "react-router-dom";

import CommentField from "../../_Btns/CommentField/CommentField";

import "./PostModal.scss";

function PostModal({ post, onClick }) {
  let [comments, setComments] = useState(post.comments);
  let handleModalClick = (e) => {
    if (e.target.classList.contains("post-modal-container")) {
      onClick();
    }
  };
  let handleCommentAdd = (comment) => {
    setComments((prevState) => [...prevState, comment]);
  };
  return (
    <div className="post-modal-container" onClick={handleModalClick}>
      <div className="post-modal">
        <img src={post.imgSrc} className="post-modal__img" alt="post img" />
        <div className="post-modal__content-container">
          <h2 className="user-modal__title">{post.title}</h2>
          <ul className="user-modal__comments">
            {comments.map((comment, index) => {
              return (
                <li className="modal-comment" key={index}>
                  <Link
                    className="modal-comment__author"
                    to={"/user" + comment.author.id}
                  >
                    {comment.author.nickname}
                  </Link>
                  <span className="modal-comment__text">: {comment.text}</span>
                </li>
              );
            })}
          </ul>
          <CommentField postId={post._id} onCommentAdd={handleCommentAdd} />
        </div>
      </div>
    </div>
  );
}

export default PostModal;
