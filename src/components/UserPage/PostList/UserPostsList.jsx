import React from "react";

import UserPost from "../Post/UserPost";

import "./UserPostsList.scss";

function UserPostsList({ postsList, userId }) {
  // console.log(postsList);

  return (
    <ul className="user__post-list">
      {postsList ? (
        <>
          {postsList.map((post) => (
            <UserPost key={post._id} post={post} userId={userId} />
          ))}
        </>
      ) : (
        <>loading</>
      )}
    </ul>
  );
}

export default UserPostsList;
