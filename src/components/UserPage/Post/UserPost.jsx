import React, { useState } from "react";

import LikeBtn from "../../_Btns/LikeBtn/LikeBtn";
import CommentsBtn from "../../_Btns/CommentsBtn/CommentsBtn";

import PostModal from "../PostModal/PostModal";

import "./UserPost.scss";

function UserPost({ post, userId }) {
  let [isPostModalShown, setIsPostModalShown] = useState(false);
  let handleCommentsBtnClick = () => {
    setIsPostModalShown((prevState) => {
      return !prevState;
    });
  };
  return (
    <>
      {isPostModalShown ? (
        <PostModal onClick={handleCommentsBtnClick} post={post} />
      ) : (
        <></>
      )}
      <li className="user__post">
        <img src={post.imgSrc} alt="user post img" className="user-post__img" />
        <div className="user-post__img-hover">
          <LikeBtn postId={post._id} likes={post.likes} />{" "}
          <CommentsBtn
            CommentsBtnClick={handleCommentsBtnClick}
            nComments={post.comments.length}
          />
        </div>
      </li>
    </>
  );
}
export default UserPost;
