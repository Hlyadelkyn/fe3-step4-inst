import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

import LikeBtn from "../../_Btns/LikeBtn/LikeBtn";
import CommentField from "../../_Btns/CommentField/CommentField";

import "./Post.scss";

function Post({ post }) {
  let userId = useSelector((store) => store.userId);
  let [isCommentsShowed, setIsCommentsShowed] = useState(false);
  let [postComments, setPostComments] = useState(post.comments);

  let toggleComments = () => {
    setIsCommentsShowed((prevState) => !prevState);
  };

  let postCommentAddHandler = (comment) => {
    setPostComments((prevState) => [...prevState, comment]);
    // console.log(comment);
  };
  return (
    <>
      <li className="post" data-testid="home-post">
        <Link className="post__author" to={"/user" + post.author.id}>
          {post.author.nickname}
        </Link>

        <img src={post.imgSrc} className="post__img" alt="post img" />

        <div className="post__actions-wrapper">
          <LikeBtn postId={post._id} likes={post.likes} />
          <CommentField
            postId={post._id}
            onCommentAdd={postCommentAddHandler}
          />
        </div>

        <div className="post__comments-wrapper">
          {postComments.length === 0 ? (
            <h2 className="post-comments__title">No Comments Yet</h2>
          ) : (
            <>
              <h2 className="post-comments__title">Comments:</h2>

              {postComments.length > 1 && isCommentsShowed ? (
                <ul className="post__comments">
                  {postComments.map((comment, id) => {
                    return (
                      <li className="post__comment" key={id}>
                        <Link
                          className="comment__author"
                          to={"/user" + comment.author.id}
                        >
                          {comment.author.nickname}
                          {postComments[postComments.length - 1].author.id ===
                            userId && <> (you)</>}
                        </Link>
                        <p className="comment__text">{comment.text}</p>
                      </li>
                    );
                  })}
                </ul>
              ) : (
                <ul className="post__comments">
                  <li className="post__comment">
                    {postComments.length === 1 ? (
                      <>
                        <Link
                          className="comment__author"
                          to={"/user" + postComments[0].author.id}
                        >
                          {postComments[0].author.nickname}
                          {postComments[postComments.length - 1].author.id ===
                            userId && <> (you)</>}
                        </Link>
                        <p className="comment__text">{postComments[0].text}</p>
                      </>
                    ) : (
                      <>
                        <Link
                          className="comment__author"
                          to={
                            "/user" +
                            postComments[postComments.length - 1].author.id
                          }
                        >
                          {
                            postComments[postComments.length - 1].author
                              .nickname
                          }
                          {postComments[postComments.length - 1].author.id ===
                            userId && <> (you)</>}
                        </Link>
                        <p className="comment__text">
                          {postComments[postComments.length - 1].text}
                        </p>
                      </>
                    )}
                  </li>
                </ul>
              )}
              {postComments.length > 1 ? (
                <>
                  {isCommentsShowed ? (
                    <button
                      className="toggle-comments"
                      onClick={toggleComments}
                    >
                      Show Less
                    </button>
                  ) : (
                    <button
                      className="toggle-comments"
                      onClick={toggleComments}
                    >
                      Show More
                    </button>
                  )}
                </>
              ) : (
                <></>
              )}
            </>
          )}
        </div>
      </li>
    </>
  );
}

export default Post;
