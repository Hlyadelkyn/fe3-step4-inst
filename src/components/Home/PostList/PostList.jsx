import React, { useEffect, useState } from "react";
import Post from "../Post/Post";
import InfiniteScroll from "react-infinite-scroll-component";

import "./PostList.scss";

function PostsList() {
  let diff = 2;
  let [posts, setPosts] = useState([]);
  let [hasMore, setHasMore] = useState(true);
  let [index, setIndex] = useState(0);

  let fetchPosts = async () => {
    // console.log(index);
    const postsData = await fetch("http://localhost:8081/posts/" + index);
    const postsJson = await postsData.json();

    setPosts([...posts, ...postsJson]);

    setIndex(index + diff);
    // console.log(posts, postsJson);
    if (postsJson === []) {
      setHasMore(false);
    }
  };

  useEffect(() => {
    // console.log(1);
    fetchPosts();
  }, []);
  return (
    <>
      {posts ? (
        <section className="home__posts-list">
          <h2 className="home-posts__title">Posts</h2>
          <InfiniteScroll
            dataLength={posts.length}
            next={fetchPosts}
            hasMore={hasMore}
            endMessage={<p className="end-title">Thats all</p>}
          >
            {posts.map((post) => {
              return <Post post={post} key={post._id} />;
            })}
          </InfiniteScroll>

          <div className="deco-line"></div>
        </section>
      ) : (
        <>
          <div className="loading-title">Posts are loading</div>{" "}
        </>
      )}
    </>
  );
}

export default PostsList;
