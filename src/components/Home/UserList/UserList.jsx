import React from "react";

import { Link } from "react-router-dom";

import "./UserList.scss";

function UserList({ users, title, currentUserId }) {
  return (
    <ul className="home__users-list">
      <h2 className="user-list__title">{title}</h2>
      {users.map((user) => {
        return (
          <li className="users-list__user" key={user._id}>
            <Link className="user__linkTo" to={"/user" + user._id}>
              {user.nickname} {user._id === currentUserId ? <>(you)</> : <></>}
            </Link>
          </li>
        );
      })}
    </ul>
  );
}

export default UserList;
