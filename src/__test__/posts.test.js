import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "../state/store";
import { createBrowserHistory } from "history";

import { Router } from "react-router-dom";

import Post from "../components/Home/Post/Post";
import UserPost from "../components/UserPage/Post/UserPost";
import PostModal from "../components/UserPage/PostModal/PostModal";

const newHistory = createBrowserHistory();
const trialPost = {
  _id: "639776adef99fddcb31f8c61",
  title: "Lorem ipsum2",
  comments: [
    {
      author: {
        id: "639482d86393bedf52b49a78",
        nickname: "boofar",
      },
      text: "Wow, brilliat!",
    },
    {
      author: {
        id: "639482d86393bedf52b49a78",
        nickname: "boofar",
      },
      text: "Lol",
    },
  ],
  author: {
    id: "639482d895d198371e1414cc",
    nickname: "foobar",
    full_name: "Foo Bar",
  },
  imgSrc: "https://picsum.photos/id/29/4000/2670",
  likes: ["639482d86393bedf52b49a78"],
};

describe("testing posts", () => {
  describe("test home post", () => {
    it("should render correctly with trial post object", () => {
      render(
        <Provider store={store}>
          <Router history={newHistory}>
            <Post post={trialPost} />
          </Router>
        </Provider>
      );
    });
  });
  describe("test userpage post", () => {
    it("should render correctly with trial post object", () => {
      render(
        <Provider store={store}>
          <Router history={newHistory}>
            <UserPost post={trialPost} userId={trialPost.author.id} />
          </Router>
        </Provider>
      );
    });
  });
  describe("test userpage post modal", () => {
    it("should render correctly", () => {
      render(
        <Provider store={store}>
          <Router history={newHistory}>
            <PostModal post={trialPost} onClick={() => {}} />
          </Router>
        </Provider>
      );
    });
  });
});
