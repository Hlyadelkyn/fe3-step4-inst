import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "../state/store";

import { Router } from "react-router-dom";

import { createBrowserHistory } from "history";

import HomePage from "../pages/Home/HomePage";
import UserPage from "../pages/User/UserPage";
import NoPage from "../pages/NoPage/NoPage";

const newHistory = createBrowserHistory();
const trialID = "trialID";

describe("test pages", () => {
  describe("test home page", () => {
    it("renders correctly", () => {
      <Provider store={store}>
        <Router history={newHistory}>
          <HomePage />
        </Router>
      </Provider>;
    });
  });
  describe("test user page", () => {
    it("renders correctly", () => {
      <Provider store={store}>
        <Router history={newHistory}>
          <UserPage />
        </Router>
      </Provider>;
    });
  });
  describe("test no-page", () => {
    it("renders correctly", () => {
      <Provider store={store}>
        <Router history={newHistory}>
          <NoPage />
        </Router>
      </Provider>;
    });
  });
});
