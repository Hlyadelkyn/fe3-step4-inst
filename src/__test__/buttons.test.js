import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "../state/store";

import { Router } from "react-router-dom";

import { createBrowserHistory } from "history";

import CommentField from "../components/_Btns/CommentField/CommentField";
import LikeBtn from "../components/_Btns/LikeBtn/LikeBtn";
import CommentsBtn from "../components/_Btns/CommentsBtn/CommentsBtn";
import ToHomeBtn from "../components/_Btns/ToHomeBtn/ToHomeBtn";

const newHistory = createBrowserHistory();
const trialID = "trialID";

describe("test btns", () => {
  describe("test comment field", () => {
    it("should render", () => {
      render(
        <Provider store={store}>
          <CommentField postId={trialID} onCommentAdd={trialID} />{" "}
        </Provider>
      );
    });
  });
  describe("test like button", () => {
    it("should render", () => {
      render(
        <Provider store={store}>
          <LikeBtn postId={trialID} likes={[trialID, trialID, trialID]} />{" "}
        </Provider>
      );
    });
  });
  describe("test comments button", () => {
    it("should render", () => {
      render(
        <CommentsBtn CommentsBtnClick={() => {}} nComments={trialID.length} />
      );
    });
  });
  describe("test to-home button", () => {
    it("should render", () => {
      render(
        <Router history={newHistory}>
          <ToHomeBtn />{" "}
        </Router>
      );
    });
  });
});

//comments-button
//like-button
