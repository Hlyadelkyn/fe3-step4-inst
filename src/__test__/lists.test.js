import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "../state/store";
import { createBrowserHistory } from "history";

import { Router } from "react-router-dom";

import PostsList from "../components/Home/PostList/PostList";
import UserList from "../components/Home/UserList/UserList";
import UserPostsList from "../components/UserPage/PostList/UserPostsList";

const newHistory = createBrowserHistory();

const trialUsers = [
  {
    _id: "639482d86393bedf52b49a78",
    nickname: "boofar",
    gender: "female",
    full_name: "Boo Far",
    subscribed: [],
    subscriptions: ["639482d895d198371e1414cc"],
    posts: [],
  },
  {
    _id: "639482d895d198371e1414cc",
    nickname: "foobar",
    gender: "male",
    full_name: "Foo Bar",
    subscribed: ["639482d86393bedf52b49a78"],
    subscriptions: [],
    posts: [
      "639482dc22a8024a930a119c",
      "639776adef99fddcb31f8c61",
      "639482dce31754d692a0c133",
    ],
  },
];
const trialPosts = [
  {
    _id: "639776adef99fddcb31f8c61",
    title: "Lorem ipsum2",
    comments: [
      {
        author: {
          id: "639482d86393bedf52b49a78",
          nickname: "boofar",
        },
        text: "Wow, brilliat!",
      },
      {
        author: {
          id: "639482d86393bedf52b49a78",
          nickname: "boofar",
        },
        text: "Lol",
      },
    ],
    author: {
      id: "639482d895d198371e1414cc",
      nickname: "foobar",
      full_name: "Foo Bar",
    },
    imgSrc: "https://picsum.photos/id/29/4000/2670",
    likes: ["639482d86393bedf52b49a78"],
  },
  {
    _id: "639482dce31754d692a0c133",
    title: "Lorem ipsum",
    comments: [],
    author: {
      id: "639482d895d198371e1414cc",
      nickname: "foobar",
      full_name: "Foo Bar",
    },
    imgSrc: "https://picsum.photos/id/12/2500/1667",
    likes: [],
  },
];
const userId = trialUsers[0]._id;

describe("testing lists", () => {
  describe("test users list", () => {
    it("should render", () => {
      render(
        <Provider store={store}>
          <Router history={newHistory}>
            <UserList
              users={trialUsers.filter((user) =>
                user.subscribed.includes(userId)
              )}
              title={"Subscriptions"}
              currentUserId={userId}
            />
          </Router>
        </Provider>
      );
    });
  });
  describe("test posts list", () => {
    it("should render", () => {
      render(
        <Provider store={store}>
          <Router history={newHistory}>
            <PostsList />
          </Router>
        </Provider>
      );
    });
  });
  describe("test user posts list", () => {
    it("should render", () => {
      render(
        <Provider store={store}>
          <Router history={newHistory}>
            <UserPostsList postsList={trialPosts.reverse()} userId={userId} />
          </Router>
        </Provider>
      );
    });
  });
});
