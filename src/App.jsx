import React, { useEffect } from "react";
import { BrowserRouter, Switch, Redirect, Route } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setUser } from "./state/actions";

import UserPage from "./pages/User/UserPage";
import HomePage from "./pages/Home/HomePage";
import NoPage from "./pages/NoPage/NoPage";

import "./App.scss";

import "./App.scss";
function App() {
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(setUser());
  }, []);
  return (
    <BrowserRouter>
      <div className="App">
        <Switch>
          <Redirect exact from="/" to="/home" />

          <Route path="/home">
            <HomePage />
          </Route>
          <Route path="/user:id" component={UserPage} />

          <Route path="/*">
            <NoPage />
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
