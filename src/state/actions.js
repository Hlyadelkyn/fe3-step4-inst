export const SET_USER = "SET_USER";

let loginId = "639482d86393bedf52b49a78";

export const setUser = () => {
  return (dispatch) => {
    fetch("http://localhost:8081/user/" + loginId)
      .then((response) => {
        if (!response.ok) {
          throw new Error("SERVER ERROR");
        } else {
          return response;
        }
      })
      .then((resp) => resp.json())
      .then((data) => {
        // console.log(data.user);
        dispatch({ type: SET_USER, payload: data.user });
      });
  };
};
// 639482dc08de039083a4877d
// fobbaf
// Fob Baf
