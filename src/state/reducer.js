import { SET_USER } from "./actions";

const initialState = {
  userId: null,
  userName: null,
  userSubscriptions: [],
};
export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        userId: action.payload._id,
        userName: action.payload.nickname,
        userSubcriptions: action.payload.subscriptions,
      };
    default:
      return state;
  }
};
