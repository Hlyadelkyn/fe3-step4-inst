import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";

import UserPostsList from "../../components/UserPage/PostList/UserPostsList";
import SubscribeBtn from "../../components/UserPage/Btns/SubscribeBtn";
import ToHomeBtn from "../../components/_Btns/ToHomeBtn/ToHomeBtn";

import "./UserPage.scss";

function UserPage() {
  let userId = useSelector((store) => store.userId);
  let [userData, setUser] = useState(null);
  const params = useParams();
  useEffect(() => {
    const fetchData = async () => {
      const data = await fetch("http://localhost:8081/user/" + params.id);
      const json = await data.json();

      setUser(json);
    };

    fetchData();
  }, []);

  if (userData) {
    return (
      <>
        <section className="user__head">
          <ToHomeBtn />
          <div className="user__content-container">
            <h2 className="user__title">user</h2>
            <h3 className="user__nickname">
              {userData.user.nickname}
              {userData.user._id === userId && (
                <span className="user-nickname__add"> (you)</span>
              )}
            </h3>

            <p className="user__fullname">{userData.user.full_name}</p>
            {userData.user.gender === "male" ? (
              <p className="user__gender">male</p>
            ) : (
              <p className="user__gender">female</p>
            )}
            {userId && (
              <SubscribeBtn
                isSubscribed={userData.user.subscribed.includes(userId)}
                id={userData.user._id}
                currentUserId={userId}
              />
            )}
          </div>
        </section>
        <section className="user__posts">
          <h2 className="user-posts__title">Posts</h2>
          {userData.userPosts.length > 0 ? (
            <UserPostsList
              postsList={userData.userPosts.reverse()}
              userId={userData.user._id}
            />
          ) : (
            <h2 className="user__no-posts">No Posts Yet :{"("}</h2>
          )}
          <div className="deco-line"></div>
        </section>
      </>
    );
  } else {
    return <>loading</>;
  }
}

export default UserPage;
