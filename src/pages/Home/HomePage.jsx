import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";

import PostsList from "../../components/Home/PostList/PostList";
import UserList from "../../components/Home/UserList/UserList";

import ToHomeBtn from "../../components/_Btns/ToHomeBtn/ToHomeBtn";

import "./HomePage.scss";

function HomePage() {
  let userId = useSelector((store) => store.userId);
  let [users, setUsers] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const usersData = await fetch("http://localhost:8081/users");
      const usersJson = await usersData.json();
      // console.log(usersJson.map((user) => user.subscribed));
      setUsers(usersJson);
    };

    fetchData();
  }, []);

  return (
    <>
      <section className="home__header">
        <ToHomeBtn />
        <h2 className="home__title">Home</h2>
      </section>
      <PostsList />

      {users && userId ? (
        <section className="home__users-lists">
          <h2 className="users-list__title">USERS</h2>
          <UserList
            users={users.filter((user) => user.subscribed.includes(userId))}
            title={"Subscriptions"}
            currentUserId={userId}
          />
          <UserList
            users={users.filter((user) => !user.subscribed.includes(userId))}
            title={"Recommendations"}
            currentUserId={userId}
          />
        </section>
      ) : (
        <>
          <div className="loading-title">Users are loading</div>{" "}
        </>
      )}
    </>
  );
}

export default HomePage;
