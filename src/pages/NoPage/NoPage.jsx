import React from "react";

import ToHomeBtn from "../../components/_Btns/ToHomeBtn/ToHomeBtn";

import "./NoPage.scss";

function NoPage() {
  return (
    <>
      <ToHomeBtn />
      <h2 className="no-page__title">No page :(</h2>
    </>
  );
}

export default NoPage;
