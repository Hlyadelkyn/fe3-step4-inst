$ npm install express --save

post prop : centeredBtns

MongoDB cheatsheet:

OVERALL ACTIONS
create or switch DB - use dbName

delete DB - db.dropDatabase()

view DB - db
.----------------------------------------------------------------

COLLECTIONS ACTIONS
show collections - show collections

create collection - db.createCollection('colectionName')

drop collection - db.colectionName.drop()
.----------------------------------------------------------------

DOCUMETS (ROWS) ACTIONS (CRUD)
show all rows in a collection - db.collectionName.find()

show first matching row - db.collectionName.findOne({props})

insert one row - db.collectionName.insert({props})

insert many rows - db.collectionName.insert([{},{}])

search - db.collectionName.find({props}) - return few

search with limit - db.collectionName.find({props}).limit(num)

find operators - db.collectionName.find({prop:{$lt, $lte, $gt, $gte : value}})

update - db.collectionName.update({filter}, {action f.e. $set{status:prop}})

users.insert([
{
id: id2,
nickname: "foobar",
gender: "man",
full_name: "Foo Bar",
subscribed: true,
subscriptions: [
{
nickname: "boofar",
id: id1,
full_name: "Boo Far",
},
{
nickname: "Fobbaf",
id: id.generate(),
full_name: "Fob Baf",
},
],
posts: [id.generate(), id.generate(), id.generate()],
},
{
id: id1,
nickname: "boofar",
gender: "man",
full_name: "Boo Far",
subscribed: false,
subscriptions: [
{
nickname: "foobar",
id: id2,
full_name: "Foo Bar",
},
{
nickname: "Fobbaf",
id: id.generate(),
full_name: "Fob Baf",
},
],
posts: [id.generate(), id.generate(), id.generate()],
},
]);
