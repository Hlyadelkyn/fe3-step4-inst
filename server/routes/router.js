const e = require("express");
const express = require("express");
const router = express.Router();

const { MongoClient, ServerApiVersion } = require("mongodb");
const client = new MongoClient(
  "mongodb+srv://hlyadelkyn:test1234@cluster0.pgqndfi.mongodb.net/?retryWrites=true&w=majority"
);

const getUsers = async () => {
  try {
    await client.connect();
    const users = client.db().collection("users");
    const usersArr = await users.find({}).toArray();
    return usersArr.reverse();
  } catch (error) {
    console.log(error);
  }
};

const getUser = async (id) => {
  try {
    await client.connect();
    const users = client.db().collection("users");

    const user = await users.findOne({ _id: id });
    const userPosts = await getUserPosts(id);

    if (user && userPosts) {
      return JSON.stringify({ user, userPosts });
    } else {
      return user;
    }
  } catch (e) {
    console.log(e);
  }
};
const getUserPosts = async (id) => {
  try {
    await client.connect();
    const posts = client.db().collection("posts");
    const userPosts = await posts.find({ "author.id": id }).toArray();
    if (userPosts) {
      return userPosts.reverse();
    } else {
      return "no posts found";
    }
  } catch (error) {
    console.log(error);
  }
};

const diff = 2;
const getPosts = async (postsLazyLoadIndex) => {
  try {
    await client.connect();
    const posts = client.db().collection("posts");
    const postsArr = await posts.find({}).toArray();

    if (postsArr) {
      if (postsArr.length > postsLazyLoadIndex) {
        if (postsArr.length - postsLazyLoadIndex >= diff) {
          postsArr.reverse();

          let tempArr = postsArr.splice(postsLazyLoadIndex, diff);
          postsLazyLoadIndex += diff;
          return tempArr;
        } else {
          postsArr.reverse();

          let tempArr = postsArr.splice(
            postsLazyLoadIndex,
            postsArr.length - postsLazyLoadIndex
          );
          postsLazyLoadIndex = postsArr.length;
          return tempArr;
        }
      } else {
        return [];
      }
    }
  } catch (error) {
    console.log(error);
  }
};

const togglesubscribtion = async (idOf, idOn, res) => {
  // console.log(idOf, " on ", idOn);
  try {
    await client.connect();
    const users = client.db().collection("users");

    //finding users
    const userIdOf = await users.findOne({ _id: idOf });
    const userIdOn = await users.findOne({ _id: idOn });

    if (userIdOf && userIdOn) {
      try {
        if (!userIdOf.subscriptions.includes(idOn)) {
          users.updateOne(
            { _id: idOf },
            { $set: { subscriptions: [...userIdOf.subscriptions, idOn] } }
          );
          users.updateOne(
            { _id: idOn },
            { $set: { subscribed: [...userIdOf.subscribed, idOf] } }
          );
          res.sendStatus(200);
        } else {
          userIdOf.subscriptions.splice(idOn, 1);
          userIdOn.subscriptions.splice(idOf, 1);
          users.updateOne(
            { _id: idOf },
            { $set: { subscriptions: userIdOf.subscriptions } }
          );
          users.updateOne(
            { _id: idOn },
            { $set: { subscribed: userIdOn.subscriptions } }
          );
          res.sendStatus(200);
        }
      } catch (error) {
        console.log.error(error);
      }
    }
  } catch (error) {
    console.log(error);
  }
};

const toggleLike = async (userId, id, res) => {
  try {
    await client.connect();
    const posts = client.db().collection("posts");
    const postToToggle = await posts.findOne({ _id: id });
    if (postToToggle) {
      try {
        if (postToToggle.likes.includes(userId)) {
          // console.log(postToToggle.likes.splice(id, 1));
          postToToggle.likes.splice(id, 1);
          posts.updateOne({ _id: id }, { $set: { likes: postToToggle.likes } });
          res.send("Item Updated!");
        } else {
          posts.updateOne(
            { _id: id },
            { $set: { likes: [...postToToggle.likes, userId] } }
          );
          res.send("Item Updated!");
        }
      } catch (error) {
        res.send(400).send("SERVER ERROR");
      }
    }
  } catch (error) {
    console.log(error);
  }
};
const addComment = async (comment, res) => {
  // console.log(comment);
  try {
    if (comment.author.id !== null) {
      await client.connect();
      const posts = client.db().collection("posts");
      const postCommentsToAdd = await posts.findOne({ _id: comment.postId });
      posts.updateOne(
        { _id: postCommentsToAdd._id },
        {
          $set: {
            comments: [
              ...postCommentsToAdd.comments,
              {
                author: {
                  id: comment.author.id,
                  nickname: comment.author.nickname,
                },
                text: comment.text,
              },
            ],
          },
        }
      );
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
};
router.get("/posts/:postsIndex", async (req, res) => {
  const posts = await getPosts(+req.params.postsIndex);
  if (posts !== null) {
    res.send(posts);
  } else {
    res.send("no posts");
  }
});

router.get("/users", async (req, res) => {
  const users = await getUsers();
  if (users !== null) {
    res.send(users);
  } else {
    res.send("no users");
  }
});

router.get("/user/:id", async (req, res) => {
  let id = req.params.id;
  const user = await getUser(id);
  if (user !== null) {
    res.send(user);
  } else {
    res.send("user not found");
    res.status(404);
  }
});

router.put("/user-togglesubscribtion/", async (req, res) => {
  // let id = req.params.id;
  togglesubscribtion(
    req.body.userToToggleSubscribtionId,
    req.body.userToToggleSubscribtionOfId,
    res
  );
});

router.put("/user-togglelike/", async (req, res) => {
  toggleLike(req.body.userId, req.body.postId, res);
});
router.put("/post-add-comment/", async (req, res) => {
  let a = await addComment(req.body);
  if (a) {
    res.send("ok").status(200);
  } else {
    res.send("error").status(400);
  }
});
module.exports = router;
