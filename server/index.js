const express = require("express");

const app = express();
const http = require("http");
const router = require("./routes/router");

const cors = require("cors");
const PORT = 8081;

const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(cors());
app.use("/", router);

http.createServer(app).listen(PORT);
