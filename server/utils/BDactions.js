import { MongoClient } from "mongodb";
const client = new MongoClient(
  "mongodb+srv://hlyadelkyn:test1234@cluster0.pgqndfi.mongodb.net/?retryWrites=true&w=majority"
);
export const subscribe = async (id) => {
  try {
    await client.connect();
    const users = client.db().collection("users");

    users.updateOne({ _id: id }, { $set: { subscribed: true } });
  } catch (e) {
    console.log(e);
  }
};

export const unsubscribe = async (id) => {
  try {
    await client.connect();
    const users = client.db().collection("users");

    users.updateOne({ _id: id }, { $set: { subscribed: false } });
  } catch (e) {
    console.log(e);
  }
};
